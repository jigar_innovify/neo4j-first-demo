class User 
	include Neo4j::ActiveNode
    #
    # Neo4j.rb needs to have property definitions before any validations. So, the property block needs to come before
    # loading your devise modules.
    #
    # If you add another devise module (such as :lockable, :confirmable, or :token_authenticatable), be sure to
    # uncomment the property definitions for those modules. Otherwise, the unused property definitions can be deleted.
    #

    property :username, :type =>   String
    property :facebook_token, :type => String
    index :facebook_token

    property :created_at, :type => DateTime
    property :updated_at, :type => DateTime

    ## Database authenticatable
    property :email, :type => String, :null => false, :default => ""
    index :email

    property :encrypted_password

    ## If you include devise modules, uncomment the properties below.

    ## Rememberable
    property :remember_created_at, :type => DateTime
    index :remember_token


    ## Recoverable
    property :reset_password_token
    index :reset_password_token
    property :reset_password_sent_at, :type =>   DateTime

    ## Trackable
    property :sign_in_count, :type => Integer, :default => 0
    property :current_sign_in_at, :type => DateTime
    property :last_sign_in_at, :type => DateTime
    property :current_sign_in_ip, :type =>  String
    property :last_sign_in_ip, :type => String
    property :name, :type => String
    has_many :in, :posts, origin: :user
    has_many :in, :comments, origin: :user

    has_many :out, :following, type: :following, unique: true, model_class: 'User'
    has_many :out, :friendship, type: :friendship, unique: true, model_class: 'User'

     ## Confirmable
     # property :confirmation_token
     # index :confirmation_token
     # property :confirmed_at, :type => DateTime
     # property :confirmation_sent_at, :type => DateTime

     ## Lockable
     #  property :failed_attempts, :type => Integer, :default => 0
     # property :locked_at, :type => DateTime
     #  property :unlock_token, :type => String,
     # index :unlock_token

    ## Token authenticatable
    # property :authentication_token, :type => String, :null => true, :index => :exact

    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable,
           :recoverable, :rememberable, :trackable, :validatable

    # following method return true if givem user email is my friend or not
    def is_friend?(to_user)
      if is_friend_requested?(to_user) && get_friend_requested?(to_user)
        true
      else
        false
      end
    end

    def is_friend_requested?(to_user)
      login_user_friend_req_uuid = self.friendship.map(&:uuid)
      login_user_friend_req_uuid.include? to_user.uuid
    end

    def get_friend_requested?(to_user)
      to_user_friend_req_uuid = to_user.friendship.map(&:uuid)
      to_user_friend_req_uuid.include? self.uuid
    end
    # following method return true if givem user email is my followers or not
    # def is_my_follower(to_user_email)
    #   friends_emails = self.friendship.map(&:email)
    #   friends_emails.include? to_user_email
    # end
   
end