class Comment
	include Neo4j::ActiveNode
  property :title
  property :text
  property :created_at, :type => DateTime
  property :updated_at, :type => DateTime
  
  index :title
  
  has_one :out, :user, type: :commented_by
  has_one :out, :post, type: :comments_on
end