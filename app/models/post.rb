class Post 
	include Neo4j::ActiveNode
  property :title
  property :text, default: 'bla bla bla'
  property :score, type: Integer, default: 0
  property :created_at, :type => DateTime
  property :updated_at, :type => DateTime
  
  validates :title, presence: true
  validates :score, numericality: { only_integer: true }

  index :title

  before_save do
    self.score = score * 100
  end
  
  has_many :in, :comments, origin: :post
  has_one :out, :user, type: :posted_by
end