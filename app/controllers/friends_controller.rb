class FriendsController < ApplicationController
	before_action :get_user_detail,:only=>[:send_friend_request,:unfriend,:friend_of_friend,:manage_friend_request,:cancel_sended_frnd_req]
	def send_friend_request
		current_user.friendship.create(@requesting_user,:from_id=>current_user.uuid,:to_id=>@requesting_user.uuid, :created => Time.now.to_i)
		redirect_to friends_path
		flash[:notice] = "Friend request was sent successfully."
	end

	def manage_friend_request
		if params[:is_accept].to_s == "0"
			# reject
			@requesting_user.friendship.delete(current_user)
			flash[:notice] = "Friend request was rejcted."
		elsif params[:is_accept].to_s == "1"
			# accept
			current_user.friendship.create(@requesting_user,:from_id=>current_user.uuid,:to_id=>@requesting_user.uuid, :created => Time.now.to_i)
			flash[:notice] = "Friend request was accepted."
		end
		redirect_to friends_path
	end

	def cancel_sended_frnd_req
		current_user.friendship.delete(@requesting_user)
		flash[:notice] = "Sended Friend request was cancel successfully."
		redirect_to friends_path
	end

	def unfriend
		current_user.friendship.delete(@requesting_user)
		@requesting_user.friendship.delete(current_user)
		redirect_to friends_path
		flash[:notice] = "Unfriend successfully."
	end

	# return friends list and un friend users also
	def user_list
		users_uuids = User.all.map(&:uuid) - current_user.uuid.split(" ")
		@users = User.where(:uuid=>users_uuids)
	end

	def friend_of_friend
		@users = @requesting_user.friendship
		@friend_of_friend = @requesting_user.friendship.friendship
		@friend_of_friend_of_friend = @requesting_user.friendship.friendship.friendship

	end

	private
	def get_user_detail
		@requesting_user = User.find_by(:uuid=>params[:id])
	end
end