# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
me = User.find_or_create_by(:name=>"me",:email=>"me@innovify.in",:password=>"123456789")
bob = User.find_or_create_by(:name=>"bob",:email=>"bob@innovify.in",:password=>"123456789")
mark = User.find_or_create_by(:name=>"mark",:email=>"mark@innovify.in",:password=>"123456789")
mary = User.find_or_create_by(:name=>"mary",:email=>"mary@innovify.in",:password=>"123456789")
john = User.find_or_create_by(:name=>"john",:email=>"john@innovify.in",:password=>"123456789")
andy = User.find_or_create_by(:name=>"andy",:email=>"andy@innovify.in",:password=>"123456789")

me.friendship.create(bob,:created=>Time.now.to_i)
bob.friendship.create(mark,:created=>Time.now.to_i) 
mark.friendship.create(mary,:created=>Time.now.to_i) 
mary.friendship.create(john,:created=>Time.now.to_i) 
john.friendship.create(andy,:created=>Time.now.to_i)